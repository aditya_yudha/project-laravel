<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Document</title> -->
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="">Full Name</label><br>
        <input type="text" name="name"><br><br>
        <label for="">Last Name</label><br>
        <input type="text" name="last_name"><br><br>
        <label for="">Gender:</label><br>
        <input type="radio">Male <br>
        <input type="radio">Female<br>
        <label for="">Nasionality :</label><br>
        <select name="nasionality" id="">
            <option value="idn">Indonesia</option>
            <option value="other">Other</option>
        </select><br><br>
        <label for="">Language Spoken : </label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <label for="">Bio : </label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit">

    </form>
</body>
</html>